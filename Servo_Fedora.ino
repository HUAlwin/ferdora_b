//Global Timer Compare Register Array's   |  comments achter de Array's zijn de uptime in microseconden
unsigned int Stop[2] = {60915,60915}; // 1500, 1500
unsigned int Links[2] = {60978, 60890}; //1750,1400
unsigned int Rechts[2] = {60852, 60940}; //1250, 1600
unsigned int Vooruit[2] = {60790,61040}; //1000, 2000
unsigned int Achteruit[2] = {61040,60790}; //2000,1000
unsigned int Linksom[2] = {60790,60790}; //1000, 1000
unsigned int Rechtsom[2] = {61040,61040}; // 2000,2000


//____________________________________
// Servo Functies
//____________________________________
ISR(TIMER1_COMPA_vect) //PORT 10
{
  PORTB &= ~(0b00000100);
}

ISR(TIMER1_COMPB_vect) //PORT 11
{
  PORTB &= ~(0b00001000);
}

ISR(TIMER1_OVF_vect)
{
  PORTB |= 0b00001100;
  TCNT1 = 60540;
}

void Tempo(unsigned int Distance_cm, unsigned int LRarray[2])
{
  if (Distance_cm <= 2)
  {
    //Stop of achteruit
  }
  else if (Distance_cm <=3)
  {
    //slowdown
  }
  else if ((Distance_cm <= 6) && (Distance_cm >=4 ))
  {
    //standaard volg snelheid
  }
  else if (Distance_cm <= 9)
  {
    //speed up
  }
  else
  OCR1B = LRarray[1];
  OCR1A = LRarray[0];
}


//____________________________________
//Sonar Functies
//____________________________________
/*int Sonar(unsigned int Echo_pulse_duration, unsigned int Distance_cm)
{
  PORTD |= 0x10; //PORT 4 HIGH
  delayMicroseconds(10); //delay of 10µs
  PORTD &= ~(0x10); //PORT 4 LOW
  Echo_pulse_duration = pulseIn(3,HIGH); //tijd in µs
  Distance_cm  = (Echo_pulse_duration * 0.0343)/2;//berekenen afstand (µs * (cm/µs)
  return(Echo_pulse_duration, Distance_cm);
}*/


//____________________________________
//Main & Settings
//____________________________________
void setup() 
{
  //sonar
 // DDRD |= 0x10;//PORT 4 OUTPUT
 // DDRD &= ~(0x08);// PORT 3 INPUT
  //servo
  DDRB |= (0b00001100);
  TCCR1A = 0; 
  TCCR1B |= 0b00000011;
  TCNT1 = 60540; //start telling
  OCR1A = 60915; //port10
  OCR1B = 60790; //port11
  TIMSK1 |= 0b00000111;
  sei();
}

void loop() 
{
  //unsigned int Echo_pulse_duration;
  unsigned int Distance_cm = 10;
  //Sonar(Echo_pulse_duration, Distance_cm);   
  Tempo(Distance_cm, Stop);
  delay(1000);
  Tempo(Distance_cm, Links);
  delay(1000);
  Tempo(Distance_cm, Rechts);
  delay(1000);
  Tempo(Distance_cm, Vooruit);
  delay(1000);
  Tempo(Distance_cm, Achteruit);
  delay(1000);
  Tempo(Distance_cm, Linksom);
  delay(1000);
  Tempo(Distance_cm, Rechtsom);
  delay(1000);
}


//____________________________________
//END
//____________________________________
