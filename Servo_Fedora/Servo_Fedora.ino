#define MINDISTANCE 15
#define MAXDISTANCE 200
#define SetDistance 50

//Global Timer Compare Register Array's   |  comments achter de Array's zijn de uptime in microseconden
unsigned int Stop[2] = {28536,28536}; // 1500, 1500
unsigned int Links[2] = {29036, 28536}; //1750,1500
unsigned int Rechts[2] = {28036, 28536}; //1250, 1500
unsigned int Vooruit[2] = {28036,29036}; //1250, 1750
unsigned int Achteruit[2] = {29036, 28036}; //1750,1250
unsigned int Linksom[2] = {29736,29736}; //1800,1800
unsigned int Rechtsom[2] = {27336,27336}; //1200,1200

unsigned int Distance_mm , Distance_mm_old;

//____________________________________
// Servo Functies
//____________________________________
ISR(TIMER1_COMPA_vect) //PORT 10
{
  PORTB &= ~(0b00000100);
}

ISR(TIMER1_COMPB_vect) //PORT 11
{
  PORTB &= ~(0b00001000);
}

ISR(TIMER1_OVF_vect)
{
  PORTB |= 0b00001100;
  TCNT1 = 25536;
}

void Tempo(unsigned int LRarray[2])
{
  if ((LRarray != Stop) || (LRarray != Rechtsom) || (LRarray != Linksom))
  {
    Sonar();
    byte Diffrents = Distance_mm_old - Distance_mm;
    if (Distance_mm < MINDISTANCE) //
    {
      OCR1B = Stop[0]; //port 11
      OCR1A = Stop[1]; //port 10
      Distance_mm_old = Distance_mm;
      Sonar();
      if (Distance_mm_old > Distance_mm)
      {
        LRarray = Achteruit;
      }
    }
    
    else if(Distance_mm < MAXDISTANCE)
    {
       if (LRarray == Vooruit)
      {
        LRarray[1] = 33536;
        LRarray[0] = 29536;
      }
      else if(LRarray == Links)
      {
        LRarray[1] = 30536;
        LRarray[0] = 33536;
      }
      else if(LRarray == Rechts)
      {
        LRarray[1] = 32536;
        LRarray[0] = 29536;
      }
    }
    else
    {
      byte adjust = (Distance_mm - SetDistance);
      int Update =(0.001*(adjust*adjust*adjust));
    }
    byte Diffrents_old = Diffrents;
  }
  OCR1B = LRarray[1]; //port 11
  OCR1A = LRarray[0]; //port 10  
}


//____________________________________
//Sonar Functies
//____________________________________
void Sonar()
{
  PORTD |= 0x10; //PORT 4 HIGH
  delayMicroseconds(10); //delay of 10µs
  PORTD &= ~(0x10); //PORT 4 LOW
  unsigned int Echo_pulse_duration = pulseIn(3,HIGH); //tijd in µs
  Distance_mm  = (Echo_pulse_duration * 0.343)/2;//berekenen afstand (µs * (mm/µs) 
}


//____________________________________
//Main & Settings
//____________________________________
void setup() 
{
  //sonar
 //DDRD |= 0x10;//PORT 4 OUTPUT
 //DDRD &= ~(0x08);// PORT 3 INPUT
  //servo
  DDRB |= (0b00001100);
  TCCR1A = 0; 
  TCCR1B = 0b00000010;
  TCNT1 = 25536; //start telling
  OCR1A = 27536; //port10
  OCR1B = 30536; //port11
  TIMSK1 = 0b00000111;
  sei();
}

void loop() 
{   
}


//____________________________________
//END
//____________________________________
